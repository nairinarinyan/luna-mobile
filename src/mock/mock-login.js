import * as firebase from 'firebase';
import config from '../config';

export default async function mockLogin() {
    const { email, password } = config.mock;
    const user = await firebase.auth().signInWithEmailAndPassword(email, password);
    return user;
}