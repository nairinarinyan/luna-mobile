import React, { Component } from 'react';
import { StackNavigator } from 'react-navigation';
import * as firebase from 'firebase';
import config from './config';

import FacilitiesContainer from './components/FacilitiesContainer';
import SpaceContainer from './components/SpaceContainer';

firebase.initializeApp(config.firebase);

const App = StackNavigator({
    Facilities: { screen: FacilitiesContainer },
    Space: { screen: SpaceContainer }
}, {
    headerMode: 'none'
});

export default App;
