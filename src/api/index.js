import * as firebase from 'firebase';

export async function get(url, json) {
    const token = await firebase.auth().currentUser.getToken();

    return fetch(url, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => json ? res.json() : res);
}