import { get } from './index';
import config from '../config';

export async function getSpaces() {
    const url  = `${config.apiUrl}/spaces`;
	return await get(url, true);
}