const roomData = [
    [4.5, 34.7, 15.4, 46.5, 'Grow Room 1'],
    [4.5, 19, 15.4, 46.5, 'Grow Room 2'],
    [4.5, 3.3, 12.5, 54, 'Grow Room 3'],
    [56, 19, 15.4, 27.8, 'Pre-Flower Room'],
    [59, 3.3, 15.5, 25, 'Veg Room'],
    [84.3, 3.3, 31.2, 8.5, 'R&D Room']
];

export default roomData;