export default {
    roomsImageUri: 'https://firebasestorage.googleapis.com/v0/b/guest-check-in-staging.appspot.com/o/map%2FSOLSTICE_map_v2_trans.png?alt=media&token=9218b3fd-69f9-4f0b-aed5-c07c15cf9c11',
    apiUrl: 'http://localhost:4000',
    customerId: 'solstice',
    firebase: {
        apiKey: "AIzaSyA7Ez96DJODUV1VOqieb9vqiqNIBkS69FI",
        authDomain: "guest-check-in-dev.firebaseapp.com",
        databaseURL: "https://guest-check-in-dev.firebaseio.com",
        storageBucket: "guest-check-in-dev.appspot.com",
        messagingSenderId: "247527772827"
    },
    mock: {
        email: 'admin@iunu.com',
        password: 'lunaraccess'
    }
}