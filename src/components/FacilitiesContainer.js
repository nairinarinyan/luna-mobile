import React, { Component } from 'react';
import { View, StyleSheet, Image, Animated, Dimensions } from 'react-native';
import config from '../config';

import mockLogin from '../mock/mock-login';
import { getSpaces } from '../api/spaces';

import FacilityMap from './FacilityMap';
import Pannable from './Pannable';
import Zoomable from './Zoomable';

export default class FacilitiesContainer extends Component {
    constructor() {
        super();
        this.state = {
            viewWidth: 0, viewHeight: 0,
            imageWidth: 0, imageHeight: 0
        };
    }

    componentDidMount() {
        mockLogin()
            .then(getSpaces)
            .then(spaces => this.setState({ spaces }));
    }

    componentDidUpdate() {
        this.image && Image.getSize(config.roomsImageUri, (originalWidth, originalHeight) => {
            const { imageWidth, imageHeight } = this.state;
            const { width, height } = this.computeImageSize(originalWidth, originalHeight);

            this.image.setNativeProps({
                style: { width, height }
            });

            if (imageWidth !== width || imageHeight !== height) {
                this.setState({
                    imageWidth: width,
                    imageHeight: height
                });
            }
        });
    }

    computeImageSize(originalWidth, originalHeight) {
        const { viewWidth, viewHeight } = this.state;
        
        const aspectRatio = originalWidth/originalHeight;

        let width = viewWidth;
        let height = width / aspectRatio;

        if (height > viewHeight) {
            height = viewHeight;
            width = height * aspectRatio;
        }
        
        return { width, height }; 
    }

    onLayout(ev) {
        const { nativeEvent: { layout: { width, height }}} = ev;
        this.setState({ viewWidth: width, viewHeight: height });
    }

    render() {
        const { spaces } = this.state;
        const { navigate } = this.props.navigation;
        
        return spaces ? (
            <View style={styles.container} onLayout={this.onLayout.bind(this)}>
                <Pannable>
                    <Zoomable>
                        <Image
                            ref={image => this.image = image}
                            source={{ uri: config.roomsImageUri }}/>
                        
                        <FacilityMap dimensions={this.state} navigate={navigate} spaces={spaces} />
                    </Zoomable>
                </Pannable>
            </View>
        ) : null;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#223c4f'
    }
});
