import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import config from '../config';

import ImageGrid from './ImageGrid';
import ImageView from './ImageView';
import LightBox from './LightBox';

export default class SpaceContainer extends Component {
    constructor() {
        super();
        this.state = {};
    }

    renderMainImage({ images_id }) {
        const mainPhotoUri = `http://luna.iunu.com/${config.customerId}/images/${images_id}/main.jpg`;
        return <ImageView uri={mainPhotoUri} />
    }

    openImage(image) {
        this.setState({
            activeImage: image 
        });
    }

    renderLightBox(image) {
        return <LightBox uri={image.imageUrl} onClose={() => {
            this.setState({ activeImage: null });
        }}/>
    }

    render() {
        const { space } = this.props.navigation.state.params;
        const { activeImage } = this.state;

        return (
            <View style={styles.container}>
                {space.images_grid ?
                    <ImageGrid space={space} openImage={this.openImage.bind(this)} /> :
                    this.renderMainImage(space)
                }

                {activeImage && this.renderLightBox(activeImage)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#223c4f'
    },
    text: {
        color: '#fff',
        fontSize: 30
    }
});