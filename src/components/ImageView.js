import React, { Component } from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import PhotoView from 'react-native-photo-view';

export default class ImageView extends Component {
    constructor() {
        super();
        this.state = {
            loading: true
        };
    }

    render() {
        const { loading } = this.state;
        const { uri } = this.props;

        return (
            <View>
                <PhotoView
                    source={{ uri }}
                    style={{ width: '100%', height: '100%' }}
                />
            </View> 
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
