import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';

import rooms from '../config/rooms';

export default class FacilityMap extends Component {
    drawRoomArea(imageWidth, imageHeight, roomData, i) {
        const keys = ['top', 'left', 'width', 'height'];

        const style = keys
            .map((key, i) => {
                const normalizer = key === 'left' || key === 'width' ? imageWidth : imageHeight;
                return { [key]: roomData[i] / 100 * normalizer };
            })
            .reduce((prev, curr) => Object.assign(prev, curr));
        
        Object.assign(style, defaultRoomStyle);
        
        return <TouchableOpacity
            key={i}
            style={style}
            onPress={this.navigateToRoom.bind(this, roomData[4])}/>
    }

    navigateToRoom(roomName) {
        const { spaces } = this.props;
        const space = spaces.find(space => space.name === roomName);
        this.props.navigate('Space', { space });
    }
    
    render() {
        const { imageWidth, imageHeight } = this.props.dimensions;
        
        return (
            <View style={styles.container}>
                {rooms.map(this.drawRoomArea.bind(this, imageWidth, imageHeight))}
            </View>
        );
    }
}

const defaultRoomStyle = {
    position: 'absolute',
    backgroundColor: 'rgba(34,60,79,0.1)',
};

const styles = StyleSheet.create({
    container: {
        position: 'absolute'
    }
});
