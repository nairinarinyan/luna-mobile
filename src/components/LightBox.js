import React, { Component } from 'react';
import { View, Image, StyleSheet, ActivityIndicator, TouchableOpacity, Dimensions, Platform } from 'react-native';
import PhotoView from 'react-native-photo-view';

export default class LightBox extends Component {
    constructor() {
        super();
        this.state = {
            loading: true
        };
    }

    getImageStyle() {
        if (Platform.OS === 'android') return styles.image;

        const { width, height } = this.state;
        if (width && height) return { width, height };

        Image.getSize(this.props.uri, (imageWidth, imageHeight) => {
            const { width } = Dimensions.get('window');
            const aspectRatio = imageWidth / imageHeight;   

            this.setState({
                width,
                height: width / aspectRatio
            });
        });

        return {}; 
    }

    onLoad() {
        this.setState({ loading: false });
    }

    render() {
        const { loading, imageWidth, imageHeight } = this.state;
        const { uri } = this.props;

        const imageStyle = { width: imageWidth, height: imageHeight };
        
        return (
            <View style={styles.container}>
                <PhotoView
                    source={{ uri }}
                    minimumZoomScale={0.5}
                    maximumZoomScale={3}
                    style={this.getImageStyle()}
                    onLoad={this.onLoad.bind(this)}
                />
                <ActivityIndicator
                    animating={loading}
                    size="large"
                    style={styles.spinner}
                    color="#7e7e7e"
                />
                <TouchableOpacity style={styles.close} onPress={this.props.onClose}>
                    <Image
                        source={require('../../assets/img/close.png')}
                        resizeMode="center"
                        style={styles.closeImage} />
                </TouchableOpacity>
            </View> 
        );
    }
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundColor: 'rgba(0, 0, 0, .6)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    spinner: {
        position: 'absolute',
    },
    image: {
        width: '100%',
        height: '100%'
    },
    close: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        top: 20,
        right: 20,
        width: 40,
        height: 40,
        borderWidth: 1,
        borderRadius: 25,
        borderColor: '#aaa'
    },
    closeImage: {
        width: 30,
        height: 30,
    }
});
