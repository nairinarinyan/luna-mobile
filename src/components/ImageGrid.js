import React, { Component } from 'react';
import { ScrollView, View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import PhotoView from 'react-native-photo-view';

import config from '../config';

const uri = 'http://luna.iunu.com/solstice/gr1/thumb.014.jpg';
const MARGIN = 3;

export default class ImageGrid extends Component {
    constructor() {
        super();
        this.state = {
            rows: [],
            imageSize: {}
        };
    }

    setImageSize(url) {
        Image.getSize(url, (width, height) => {
            if (this.state.imageSize.width !== width) {
                this.setState({ imageSize: { width, height } });
            }
        }, err => {
            console.log(err);
            if (this.state.imageSize && this.state.imageSize.width !== 300) {
                this.setState({ imageSize: { width: 300, height: 150 } });
            }
        });
    }

    renderGrid() {
        const { images_id, images_grid: { x, y } } = this.props.space;
        const rows = [];

        for (let i = 0; i < x * y; i++) {
            const prefix = i < 9 ? '00' : '0';
            const imageName = prefix + (i + 1);

            const thumbnailUrl = `http://luna.iunu.com/${config.customerId}/${images_id}/thumb.${imageName}.jpg`;
            const imageUrl = `http://luna.iunu.com/${config.customerId}/${images_id}/${imageName}.jpg`;

            if (i === 5) this.setImageSize(thumbnailUrl);

            const tileImage = { thumbnailUrl, imageUrl };

            if (!(i % x)) {
                rows.push([tileImage]);
            } else {
                rows[rows.length - 1].push(tileImage);
            }
        }

        return this.renderRows(rows);
    }

    renderRows(rows) {
        const { imageSize } = this.state;

        if (!imageSize) return;

        return rows.map((row, i) =>
            <View style={styles.row} key={i}>{this.renderTileImages(row)}</View>
        );
    }

    renderTileImages(row) {
        const { width, height } = this.state.imageSize;
        const { x } = this.props.space.images_grid;
        const { width: viewWidth } = Dimensions.get('window');

        const aspectRatio = width / height;

        const imageWidth = viewWidth / x - 2 * MARGIN;
        const imageHeight = imageWidth / aspectRatio;

        const imageStyle = { width: imageWidth, height: imageHeight };

        return row.map((tile, i) =>
            <TouchableOpacity key={i} onPress={this.props.openImage.bind(this, tile)}>
                <Image style={imageStyle} source={{ uri: tile.thumbnailUrl, cache: 'reload' }} />
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {this.renderGrid()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    row: {
        flex: 1,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: MARGIN,
        marginBottom: MARGIN
    },
    image: {
        width: 60,
        height: 50
        // flex: .1
    }
});
