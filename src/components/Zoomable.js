import React from 'react';
import { View, PanResponder, Animated, StyleSheet } from 'react-native';

// TODO
export default class Zoomable extends React.Component {
    constructor() {
        super();
        this.state = {
            scale: new Animated.Value(1)
        };
    }

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: evt => true,
            onMoveShouldSetPanResponder: evt => true,
            onPanResponderTerminationRequest: evt => true,
            onPanResponderGrant: (evt, gestureState) => {
            },
            onPanResponderMove: (evt, gestureState) => {
            },
            onPanResponderRelease: (evt, gestureState) => {
            }     
        });
    }

    render() {
        return (
            <Animated.View {...this.panResponder.panHandlers}>
                {this.props.children}
            </Animated.View>
        );
    }
}
