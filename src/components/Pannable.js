import React from 'react';
import { View, PanResponder, Animated, StyleSheet } from 'react-native';

export default class Pannable extends React.Component {
    constructor() {
        super();
        this.state = {
            pan: new Animated.ValueXY()
        };
    }

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onStartShouldSetPanResponder: evt => true,
            onMoveShouldSetPanResponder: evt => true,
            onPanResponderTerminationRequest: evt => true,
            onPanResponderGrant: (evt, gestureState) => {
                const { x, y } = this.state.pan;
                this.state.pan.setOffset({ x: x._value, y: y._value });
                this.state.pan.setValue({ x: 0, y: 0 });
            },
            
            onPanResponderMove: Animated.event([null, {
                dx: this.state.pan.x,
                dy: this.state.pan.y
            }]),
            
            onPanResponderRelease: (evt, gestureState) => {
                this.state.pan.flattenOffset();
            }
        });
    }

    render() {
        return (
            <Animated.View {...this.panResponder.panHandlers}
                style={{
                    transform: this.state.pan.getTranslateTransform()
                }}>
                {this.props.children}
            </Animated.View>
        );
    }
}
